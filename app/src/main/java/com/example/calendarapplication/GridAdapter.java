package com.example.calendarapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

// Адаптер для правильного отображения дней месяца
public class GridAdapter extends ArrayAdapter
{
    List<Date> dates;
    Calendar currentDate;
    List<Events> events;
    LayoutInflater inflater;
    public GridAdapter(@NonNull Context context, List<Date> dates,Calendar currentDate,
    List<Events> events)
    {
        super(context, R.layout.single_cell_layout);
        this.dates = dates;
        this.currentDate = currentDate;
        this.events = events;
        inflater = LayoutInflater.from(context);
    }


    // Создание ячейки
    @NonNull
    @Override
    // Компановка одного View
    public View getView(int position, @Nullable View convertView,
                        @NonNull ViewGroup parent)
    {

        Date mothDate = dates.get(position);
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(mothDate);
        int DayNo = dateCalendar.get(Calendar.DAY_OF_MONTH);
        int displayMoth = dateCalendar.get(Calendar.MONTH)+1;
        int displayYear = dateCalendar.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH)+1;
        int currentYear = currentDate.get(Calendar.YEAR);

        View view  = convertView;
        if(view == null)
        {
            view = inflater.inflate(R.layout.single_cell_layout,parent,false);
        }
        if (displayMoth == currentMonth && displayYear ==currentYear)
        {
            view.setBackgroundColor(getContext().getResources().getColor(R.color.blue));
            Calendar calendar = Calendar.getInstance();
            if (DayNo == calendar.get(Calendar.DAY_OF_MONTH) && currentMonth == calendar.get(Calendar.MONTH)+1 && currentYear == calendar.get(Calendar.YEAR))
            {
                view.setBackgroundColor(getContext().getResources().getColor(R.color.light_blue));
            }
        }
        else
        {
            view.setBackgroundColor(getContext().getResources().getColor(R.color.white));
        }

        //Установка значения дня
        TextView Day_Number = view.findViewById(R.id.calendar_day);
        TextView EventNumber = view.findViewById(R.id.events_id);
        Day_Number.setText(String.valueOf(DayNo));
        Calendar eventCalendar = Calendar.getInstance();
        //Работа с событиями
        ArrayList<String> arrayList = new ArrayList<>();
        Log.i("What About","Current size of events: "+events.size());

        for (int i =0; i< events.size();i++)
        {
            eventCalendar.setTime(ConvertStringToDate(events.get(i).getDATE()));
            if(DayNo == eventCalendar.get(Calendar.DAY_OF_MONTH) && displayMoth == eventCalendar.get(Calendar.MONTH) +1
            && displayYear == eventCalendar.get(Calendar.YEAR))
            {
                arrayList.add(events.get(i).getEVENT());
                EventNumber.setText("Задач:" + arrayList.size() );
                Log.i("What About Added","Added");
            }


        }
        return view;
    }

    private  Date ConvertStringToDate(String eventDate)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", new Locale("ru","RU"));
        Date date = null;
        try
        {
            date = format.parse(eventDate);
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
    return date;
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public int getPosition(@Nullable Object item)
    {
        return dates.indexOf(item);
    }

    @Nullable
    @Override
    public Object getItem(int position)
    {
        return dates.get(position);
    }
}
