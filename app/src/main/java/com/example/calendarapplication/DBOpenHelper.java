package com.example.calendarapplication;

import android.app.usage.UsageEvents;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

// База данных, для дальнейшего хранения заметок
public class DBOpenHelper extends SQLiteOpenHelper
{

    // Запрос на создание таблицы
    private final static String CREATE_EVENTS_TABLE = "create table " + AppData.EVENT_NAME +
            " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " + AppData.EVENT+ " TEXT, " +AppData.TIME+" TEXT, "
            + AppData.DATE+" TEXT, "+AppData.MONTH+" TEXT, "+AppData.YEAR+" TEXT)";

    // Запрос на удаление таблицы
    private static final  String DROP_EVENTS_TABLE = "DROP TABLE IF EXISTS " + AppData.DB_NAME;

    //Конструктор
    public DBOpenHelper(@Nullable Context context)
    {
        super(context, AppData.DB_NAME, null, AppData.DB_VERSION );
    }

    // Создание БД
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_EVENTS_TABLE);
    }

    //Обнавление БД
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL(DROP_EVENTS_TABLE);
        onCreate(db);
    }

    // Функция добавления нового события
    public void SaveEvent (String event, String time, String date, String month, String year, SQLiteDatabase datebase)
    {
        ContentValues cv = new ContentValues();
        cv.put(AppData.EVENT, event);
        cv.put(AppData.TIME,time);
        cv.put(AppData.DATE,date);
        cv.put(AppData.MONTH,month);
        cv.put(AppData.YEAR,year);
        datebase.insert(AppData.EVENT_NAME,null,cv);
    }

    // Функция чтения события
    public Cursor ReadEvents(String date, SQLiteDatabase database)
    {
        // 1 - переменная - массив возвращ. знач, 2 - столбец по заросу, 3- массив сравниваемых данных
        String [] ElementsOfTable = {AppData.EVENT,AppData.TIME,AppData.DATE,AppData.MONTH,AppData.YEAR};
        String Selection = AppData.DATE+ " =?";
        String [] SelectionArgs = {date};
        return database.query(AppData.EVENT_NAME,ElementsOfTable,Selection, SelectionArgs,null,null,null);
    }

    public Cursor ReadEventsperMonth(String moth,String year, SQLiteDatabase database)
    {
        // 1 - переменная - массив возвращ. знач, 2 - столбец по заросу, 3- массив сравниваемых данных
        String [] ElementsOfTable = {AppData.EVENT,AppData.TIME,AppData.DATE,AppData.MONTH,AppData.YEAR};
        String Selection = AppData.MONTH+ " =? and "+ AppData.YEAR+" =?";
        String [] SelectionArgs = {moth,year};
        return database.query(AppData.EVENT_NAME,ElementsOfTable,Selection, SelectionArgs,null,null,null);
    }

    public void deleteEvent(String event,String date,String time,SQLiteDatabase database){
        String selection = AppData.EVENT+"=? and "+ AppData.DATE+"=? and "+AppData.TIME+"=?";
        String[] selectionArg = {event,date,time};
        database.delete(AppData.EVENT_NAME,selection,selectionArg);
    }
}
