package com.example.calendarapplication;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class CalendarView extends LinearLayout
{
    Spinner months,years;
    ArrayAdapter<?> adapter;
    StatemantAdapter statemantAdapter;
    ListView statemant;

    ImageButton nextMonth, previosMonth;
    TextView date;
    GridView gridView;
    private static final int MAX_CALENDAR_DAYS = 42;
    private static final int MAX_STATEMANTS = 6;
    // Задание элементам необходимых форматов вывода
    SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("MMMM yyyy", new Locale("ru","RU"));
    SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM", new Locale("ru","RU") );
    SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy",new Locale("ru","RU"));
    SimpleDateFormat dateFormat  = new SimpleDateFormat("yyyy-MM-dd", new Locale("ru","RU"));
    Calendar calendar = Calendar.getInstance(new Locale("ru","RU"));
    Context context;

    GridAdapter gridAdapter;
    AlertDialog alertDialog;
    List<Date> dates = new ArrayList<>();
    List<Events> eventList = new ArrayList<>();
    DBOpenHelper dbOpenHelper;

    public CalendarView(Context context) {
        super(context);
    }

    public CalendarView(final Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        Log.i("My App", "ok, goodWork");
        InitialiseLayout();
        SetUpCalendar();
        previosMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH, -1);
                months.setSelection(calendar.get(Calendar.MONTH));
                years.setSelection(calendar.get(Calendar.YEAR)-1970);
                SetUpCalendar();
            }
        });

        nextMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar.add(Calendar.MONTH, 1);
                months.setSelection(calendar.get(Calendar.MONTH));
                years.setSelection(calendar.get(Calendar.YEAR)-1970);
                SetUpCalendar();
            }
        });

        months.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                calendar.set(Calendar.MONTH,position);
                SetUpCalendar();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        years.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                calendar.set(Calendar.YEAR, position+1970);
                SetUpCalendar();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            // Вывод даилогового окна при нажатии на дату
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                AlertDialog.Builder builder =new AlertDialog.Builder(context);
            builder.setCancelable(true);
            // Создаём сложный View
            final View addView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_new_event_layout,null);
            final EditText EventName = addView.findViewById(R.id.event);
            final TextView EventTime = addView.findViewById(R.id.eventtime);
            ImageButton SetTime = addView.findViewById(R.id.seteventtime);
            Button AddEvent = addView.findViewById(R.id.addevent);
            SetTime.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // создание диалогового окна с выбором необходимого времени(рассмотреть Spinner)
                    Calendar calendar = Calendar.getInstance();
                    int hours = calendar.get(Calendar.HOUR_OF_DAY);
                    int minuts = calendar.get(Calendar.MINUTE);
                    TimePickerDialog timePickerDialog = new TimePickerDialog(addView.getContext(),
                            R.style.Theme_AppCompat_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute)
                        {
                            // Создали пикер, выбрали время, перевели в необходимый формат, присвоили EventTime соответствующее значении
                            Calendar c = Calendar.getInstance();
                            c.set(Calendar.HOUR_OF_DAY,hourOfDay);
                            c.set(Calendar.MINUTE,minute);
                            c.setTimeZone(TimeZone.getDefault());
                            SimpleDateFormat hformate = new SimpleDateFormat("k:mm", new Locale("ru","RU"));
                            String event_Time = hformate.format(c.getTime());
                            EventTime.setText(event_Time);
                        }
                    },hours,minuts, true);
                    timePickerDialog.show();
                }
            });
            // запомнили дату, которую выбрали
            final String date = dateFormat.format(dates.get(position));
            final String month = monthFormat.format(dates.get(position));
            final String year = yearFormat.format(dates.get(position));
            // сохранение добавленного события
            AddEvent.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    SaveEvent(EventName.getText().toString(), EventTime.getText().toString(), date, month,year);
                    SetUpCalendar();
                    alertDialog.dismiss();
                }
            });

            // Отображение окна диалога
            builder.setView(addView);
            alertDialog = builder.create();
            alertDialog.show();

            }

        });
        // Открытие списка задач, при долгом удержании на ячейки с задачей
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                String date = dateFormat.format(dates.get(position));

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(true);
                View showView = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_events_layout,null);
                RecyclerView recyclerView = (RecyclerView)showView.findViewById(R.id.EventsRV);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(showView.getContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(true);

                EventsRecyclerAdapter eventsRecyclerAdapter = new EventsRecyclerAdapter(showView.getContext(),CollectEvent(date));
                recyclerView.setAdapter(eventsRecyclerAdapter);
                eventsRecyclerAdapter.notifyDataSetChanged();

                builder.setView(showView);
                alertDialog = builder.create();
                alertDialog.show();
                alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {
                        SetUpCalendar();
                    }
                });
                return true;
            }
        });
    }

    // Возврат множества событий, прикреплённых к заданному числу
    private ArrayList<Events> CollectEvent(String date)
    {
        ArrayList<Events> arrayList = new ArrayList<>();
        dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase database = dbOpenHelper.getReadableDatabase();
        Cursor cursor = dbOpenHelper.ReadEvents(date,database);
        while(cursor.moveToNext()) {
            String event = cursor.getString(cursor.getColumnIndex(AppData.EVENT));
            String time = cursor.getString(cursor.getColumnIndex(AppData.TIME));
            String Date = cursor.getString(cursor.getColumnIndex(AppData.DATE));
            String month = cursor.getString(cursor.getColumnIndex(AppData.MONTH));
            String year = cursor.getString(cursor.getColumnIndex(AppData.YEAR));
            Events events = new Events(event, time, Date, month, year);
            arrayList.add(events);
        }
        cursor.close();
        dbOpenHelper.close();
        Toast.makeText(context, String.valueOf(arrayList.size()), Toast.LENGTH_SHORT).show();
        return arrayList;
    }


    public CalendarView (Context context, @Nullable AttributeSet attrs, int defStyAttr)
    {
        super(context,attrs,defStyAttr);
    }

    // Функция для сохранения нового события
    private void SaveEvent(String event, String time, String date, String month, String year)
    {
        dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        dbOpenHelper.SaveEvent(event,time,date,month,year,database);
        dbOpenHelper.close();
        Toast.makeText(context, "Событие сохранено", Toast.LENGTH_SHORT).show();
    }

    // Получаем элементы activity_mail_layout
    private void InitialiseLayout()
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_main_layout,this);
        nextMonth = view.findViewById(R.id.nextMonth);
        previosMonth = view.findViewById(R.id.prevMonth);
        date = view.findViewById(R.id.currentDate);
        gridView = view.findViewById(R.id.gridView);
        years = view.findViewById(R.id.years);
        months = view.findViewById(R.id.months);
        statemant= view.findViewById(R.id.statemant);
        FillMonths();
        FillYears();
    }

    // Инициализация выбранной даты currentDate, заполнение списка дат, днями нового месяца
    private void SetUpCalendar()
    {


        String currentDate = simpleDateFormat.format(calendar.getTime());
        date.setText(currentDate);
        dates.clear();
        Calendar monthCalendar = (Calendar)calendar.clone();
        monthCalendar.set(Calendar.DAY_OF_MONTH,1);
        int firstDayOfMoth = monthCalendar.get(Calendar.DAY_OF_WEEK)-2 ;
        monthCalendar.add(Calendar.DAY_OF_MONTH,-firstDayOfMoth);
        CollectEventsPerMonth(monthFormat.format(calendar.getTime()), yearFormat.format(calendar.getTime()));

        while(dates.size()<MAX_CALENDAR_DAYS)
        {
            dates.add(monthCalendar.getTime());
            monthCalendar.add(Calendar.DAY_OF_MONTH,1);
        }

        gridAdapter = new GridAdapter(context, dates, calendar, eventList);
        gridView.setAdapter(gridAdapter);
        statemantAdapter = new StatemantAdapter(context,ParityWeekCount(), WeekCount());
        statemant.setAdapter(statemantAdapter);
    }

    // Чтение событий из базы данныъ
    private void CollectEventsPerMonth(String Month, String Year)
    {
        eventList.clear();
        dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor cursor = dbOpenHelper.ReadEventsperMonth(Month,Year,db);
        while (cursor.moveToNext())
        {
            String event = cursor.getString(cursor.getColumnIndex(AppData.EVENT));
            String time = cursor.getString(cursor.getColumnIndex(AppData.TIME));
            String date = cursor.getString(cursor.getColumnIndex(AppData.DATE));
            String month = cursor.getString(cursor.getColumnIndex(AppData.MONTH));
            String year = cursor.getString(cursor.getColumnIndex(AppData.YEAR));
            Events events = new Events(event,time,date,month,year);
            eventList.add(events);
        }
        /*for (int i = 0 ; i < eventList.size();i++)
        {
            dbOpenHelper.deleteEvent( eventList.get(i).getEVENT(), eventList.get(i).getDATE(),eventList.get(i).getTIME(),db);
        }*/
        cursor.close();
        dbOpenHelper.close();
    }


    public void FillMonths()
    {
        Calendar currentDate = Calendar.getInstance();
        adapter = ArrayAdapter.createFromResource(context, R.array.months, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        months.setAdapter(adapter);
        months.setSelection(currentDate.get(Calendar.MONTH)-1);
    }

    public void FillYears()
    {
        Calendar currentDate = Calendar.getInstance();
        int selectedYear = currentDate.get(Calendar.YEAR) - 1970;
        ArrayList<String> initYears = new ArrayList();
        Integer counter = 1970;
        while (counter < 2033) {
            initYears.add(Integer.toString(counter));
            counter++;
        }
        adapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item,initYears);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        years.setAdapter(adapter);
        years.setSelection(selectedYear);
    }


    private List<String> ParityWeekCount()
    {
        List<String> listOfStatemants = new ArrayList<String>();
        Calendar date = Calendar.getInstance();
        date.set(Calendar.MONTH, 8);
        if((calendar.get(Calendar.MONTH)>=8)&&(calendar.get(Calendar.MONTH)<=11)){
            date.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        }
        else date.set(Calendar.YEAR, calendar.get(Calendar.YEAR)-1);// берём сентябрь за прошлый год
        date.set(Calendar.DAY_OF_MONTH, 1);
        int dayWeek = date.get(Calendar.DAY_OF_WEEK);//день недели
        int flagPrevious;
        boolean parity_state = false;
        while(date.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)){
            if(parity_state) parity_state = false;
            else parity_state = true;
            date.add(Calendar.DAY_OF_MONTH, 7);
        }
        date.set(Calendar.DAY_OF_MONTH, 1);
        if(date.get(Calendar.DAY_OF_WEEK) > dayWeek) {
            flagPrevious = 1;
            if(parity_state)
            {
                listOfStatemants.add(0,"Нч");
            }
            else listOfStatemants.add(0,"Чт");
            date.add(Calendar.DAY_OF_MONTH, 7);
        }
        else{
            if (date.get(Calendar.DAY_OF_WEEK) == 1) {
                flagPrevious = 1;
                if(parity_state)
                {
                    listOfStatemants.add(0,"Нч");
                }
                else  listOfStatemants.add(0,"Чт");
                date.add(Calendar.DAY_OF_MONTH, 7);
            }
            else {
                flagPrevious = 0;
            }
        }
        for (int i = flagPrevious; i<6; i++){
            //Log.d(LOG_TAG, date.get(Calendar.DAY_OF_MONTH) + " day");
            if(date.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)){
                if(parity_state){
                    listOfStatemants.add("Чт");
                    parity_state = false;
                }
                else{
                    listOfStatemants.add("Нч");
                    parity_state = true;
                }
            }
            else if((date.get(Calendar.DAY_OF_MONTH)>=1)&&(date.get(Calendar.DAY_OF_MONTH)<=6)&&((date.get(Calendar.DAY_OF_WEEK)<2)||(date.get(Calendar.DAY_OF_WEEK)>(date.get(Calendar.DAY_OF_MONTH)+1)))){
                if(parity_state){
                    listOfStatemants.add("Чт");
                }
                else{
                    listOfStatemants.add("Нч");
                }
            }
            else{
                listOfStatemants.add(" ");
            }
            date.add(Calendar.DAY_OF_MONTH,7);

        }
        return listOfStatemants;
    }

    private List<String> WeekCount()
    {
        List<String> weekCounts = new ArrayList<String>();
        Calendar date = Calendar.getInstance();
        date.set(Calendar.MONTH, 0);
        date.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        date.set(Calendar.DAY_OF_MONTH, 1);
        int dayWeek = date.get(Calendar.DAY_OF_WEEK);
        int count = 1;
        while(date.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)){
            count++;
            date.add(Calendar.DAY_OF_MONTH, 7);
        }
        int flagPrevious;
        date.set(Calendar.DAY_OF_MONTH, 1);
        if(date.get(Calendar.DAY_OF_WEEK) > dayWeek) {
            flagPrevious = 1;
            weekCounts.add(0,(Integer.toString(count - 1))+":");
            //numberWeek[0].setText(Integer.toString(count - 1));
            date.add(Calendar.DAY_OF_MONTH,7);
        }
        else{
            if (date.get(Calendar.DAY_OF_WEEK) == 1) {
                flagPrevious = 1;
                //numberWeek[0].setText(Integer.toString(count - 1));
                weekCounts.add(0,(Integer.toString(count - 1))+":");
                date.add(Calendar.DAY_OF_MONTH,7);
            }
            else {
                flagPrevious = 0;
            }
        }
        for (int i = flagPrevious; i<6; i++){
            if(date.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)){
                weekCounts.add(i,(Integer.toString(count))+":");
                //numberWeek[i].setText(Integer.toString(count));
            }
            else if((date.get(Calendar.DAY_OF_MONTH)>=1)&&(date.get(Calendar.DAY_OF_MONTH)<=6)&&((date.get(Calendar.DAY_OF_WEEK)<2)||(date.get(Calendar.DAY_OF_WEEK)>(date.get(Calendar.DAY_OF_MONTH)+1)))){
                weekCounts.add(i,(Integer.toString(count))+":");
                //numberWeek[i].setText(Integer.toString(count));
            }
            else{
                weekCounts.add(" ");
                //numberWeek[i].setText(" ");
            }
            date.add(Calendar.DAY_OF_MONTH,7);
            count++;
        }
        return weekCounts;
    }

}

