package com.example.calendarapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class EventsRecyclerAdapter extends RecyclerView.Adapter<EventsRecyclerAdapter.ViewHolder>
{

    Context context;
    ArrayList<Events> arrayList;
    DBOpenHelper dbOpenHelper;

    public EventsRecyclerAdapter(Context context, ArrayList<Events> arrayList)
    {
        this.context = context;
        this.arrayList = arrayList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_row_events_layout,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Events events = arrayList.get(position);
        holder.Time.setText(events.getTIME());
        holder.Event.setText(events.getEVENT());
        holder.Date.setText(events.getDATE());
        holder.delete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DeletaCalendarEvent(events.getEVENT(),events.getDATE(),events.getTIME());
                arrayList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }
// Класс для захвата отдельного элемента в списке событий

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView Date,Event,Time;
        Button delete;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            Date = itemView.findViewById(R.id.date);
            Event = itemView.findViewById(R.id.event);
            Time = itemView.findViewById(R.id.time);
            delete = itemView.findViewById(R.id.delete);
        }
    }

    private void DeletaCalendarEvent(String event,String date,String time)
    {
        dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        dbOpenHelper.deleteEvent(event,date,time,database);
        dbOpenHelper.close();
    }


}
