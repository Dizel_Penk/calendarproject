package com.example.calendarapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class StatemantAdapter extends ArrayAdapter
{
    List<String> statements;
    List<String> numWeek;
    LayoutInflater inflater;
    public StatemantAdapter (@NonNull Context context, List<String> statemants, List<String> contWeeks)
    {
        super(context, R.layout.single_cell_layout);
        this.statements = statemants;
        this.numWeek = contWeeks;
        inflater = LayoutInflater.from(context);
    }

    public StatemantAdapter(@NonNull Context context, int resource)
    {
        super(context, resource);
    }

    // Создание ячейки
    @NonNull
    @Override
    // Компановка одного View
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {

        String state = statements.get(position);
        String week = numWeek.get(position);
        View view  = convertView;
        if(view == null)
        {
            view = inflater.inflate(R.layout.statemant_layout,parent,false);
        }
        TextView stateText = view.findViewById(R.id.state);
        stateText.setText(week+" "+state);
        view.setBackgroundColor(getContext().getResources().getColor(R.color.purple));
        return view;
    }
    @Override
    public int getCount() {
        return statements.size();
    }

    @Override
    public int getPosition(@Nullable Object item)
    {
        return statements.indexOf(item);
    }

    @Nullable
    @Override
    public Object getItem(int position)
    {
        return statements.get(position);
    }
}
